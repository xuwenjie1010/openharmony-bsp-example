# openharmony移植示例

#### 介绍
openharmony移植示例
基于最新的master版本，目前支持的芯片有：

GD32F303

GD32F450

目标是移植更多的开发板更多的MCU支持OpenHarmony


经过几天的努力，终于成功把最新版本的OpenHarmony轻量系统内核移植到ARM单片机上，现在代码开源、移植文章也完成7篇，后续会考虑增加视频讲解，直播。以便大家也可以自己移植。

[移植OpenHarmony轻量系统【1】移植思路](https://harmonyos.51cto.com/posts/10206)

[移植OpenHarmony轻量系统【2】Board和SoC解耦的设计思路](https://harmonyos.51cto.com/posts/10219)

[移植OpenHarmony轻量系统【3】Board、SOC、架构与代码对应关系](https://harmonyos.51cto.com/posts/10295)

[移植OpenHarmony轻量系统【4】启动文件与链接](https://harmonyos.51cto.com/posts/10325)

[移植OpenHarmony轻量系统【5】newlibc库移植](https://harmonyos.51cto.com/posts/10326)

[移植OpenHarmony轻量系统【6】内核初始化和启动](https://harmonyos.51cto.com/posts/10327)

[移植OpenHarmony轻量系统【7】烧录与验证](https://harmonyos.51cto.com/posts/10328)

## 代码仓库

代码仓库如下：

![image.png](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202202/e51a76e5648dec1cb42555b42376aff62bdbe1.png?x-oss-process=image/resize,w_554,h_335)

其中，01_vendor_soc_board 是初步移植的示例，编译不通过

02_vendor_soc_board是已经可以编译通过并且烧录到GD32F303上可以正常跑的。

## 使用说明

### （1）代码下载

开发者可以直接先下载最新的openharmony代码，参考文章：

https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-sourcecode-acquire.md 

然后下载本仓库的代码，将对应的代码拷贝到device/board 、device/soc、vendor中

![image.png](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202202/7836b76809d5535d7d69115abf4c9c7823aa33.png?x-oss-process=image/resize,w_319,h_108)

### （2）交叉编译器下载：

我们使用的编译器是arm-none-eabi-gcc，下载地址：

git clone https://gitee.com/harylee/gcc-arm-none-eabi-10-2020-q4-major.git

将交叉编译器环境变量bin目录配置到.bashrc文件中。

执行arm-none-eabi-gcc -v，有如下打印则表示交叉编译器配置正确。

![image.png](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202202/99e2b0259cfc756483669985b445a8b89ad6da.png?x-oss-process=image/resize,w_554,h_245)

### （3）编译
执行hb set，选择gd32f303_lianzhian

![image.png](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202202/53249cb126b79d79a53783e69982884bff7dc8.png?x-oss-process=image/resize,w_181,h_40)

然后执行hb build -f，如下提示，则表示编译成功

![image.png](https://harmonyos.oss-cn-beijing.aliyuncs.com/images/202202/e2e979f154a11a610ba404e546c99627c37f62.png?x-oss-process=image/resize,w_554,h_189)



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
