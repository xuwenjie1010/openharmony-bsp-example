/**
  ******************************************************************************
  * @file    gpio.c
  * @brief   This file provides code for the configuration
  *          of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gd32f30x.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as
        * Analog
        * Input
        * Output
        * EVENT_OUT
        * EXTI
*/
void GPIO_Init(void)
{
  rcu_periph_clock_enable(RCU_GPIOF);
}


void led_init(void)
{
    rcu_periph_clock_enable(RCU_GPIOF);
    gpio_init(GPIOF, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ,GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
}



void led_on(int lednum)
{
	switch(lednum)
	{
		case 0:
    		gpio_bit_set(GPIOF,GPIO_PIN_0);
			break;
			
		case 1:
    		gpio_bit_set(GPIOF,GPIO_PIN_1);
			break;
			
		case 2:
    		gpio_bit_set(GPIOF,GPIO_PIN_2);
			break;

		case 3:
    		gpio_bit_set(GPIOF,GPIO_PIN_3);
			break;
	}
}


void led_off(int lednum)
{
	switch(lednum)
    {
		case 0:
    		gpio_bit_reset(GPIOF,GPIO_PIN_0);
			break;
			
		case 1:
    		gpio_bit_reset(GPIOF,GPIO_PIN_1);
			break;
			
		case 2:
    		gpio_bit_reset(GPIOF,GPIO_PIN_2);
			break;

		case 3:
    		gpio_bit_reset(GPIOF,GPIO_PIN_3);
			break;
	}
}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
