#include "los_config.h"
#include "los_debug.h"
#include "los_interrupt.h"
#include "los_task.h"
#include "los_tick.h"

VOID TaskSample(VOID);

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
	UINT32 ret;

	//串口初始化
	USART1_UART_Init();
	
	//内核初始化
    ret = LOS_KernelInit();

    printf("Lian Zhian 13510979604\r\n");
    printf("Open Harmony 3.3 start ...\r\n\r\n");

    if (ret == LOS_OK) {
#if (LOSCFG_USE_SHELL == 1)
        LosShellInit();
        OsShellInit();
#endif
		//创建自己的线程
        TaskSample();
        //启动OpenHarmony OS 内核
        LOS_Start();
    }
	
	while (1)
	{

	}
  /* USER CODE END 3 */
}

//线程2，用于点灯和打印
VOID TaskSampleEntry2(VOID)
{
	printf("______>>>>>>>>> %s %d\r\n", __FILE__, __LINE__);
	led_init();
    while (1) {
        printf("___>>>>>> %s %d\r\n", __FILE__, __LINE__);
		
		led_on(0);
		led_on(1);
		led_on(2);
		led_on(3);
        LOS_TaskDelay(1000);
		
		led_off(0);
		led_off(1);
		led_off(2);
		led_off(3);
		LOS_TaskDelay(1000);
    }
}

//线程1，用于打印
VOID TaskSampleEntry1(VOID)
{
	printf("______>>>>>>>>> %s %d\r\n", __FILE__, __LINE__);
    while (1) {
        printf("___>>>>>> %s %d\r\n", __FILE__, __LINE__);
        LOS_TaskDelay(1000);
    }
}

//创建线程
VOID TaskSample(VOID)
{
    UINT32 uwRet;
    UINT32 taskID1;
    UINT32 taskID2;
	UINT32 taskID3;
    TSK_INIT_PARAM_S stTask = {0};

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)TaskSampleEntry1;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "TaskSampleEntry1";
    stTask.usTaskPrio = 6; /* Os task priority is 6 */
    uwRet = LOS_TaskCreate(&taskID1, &stTask);
    if (uwRet != LOS_OK) {
        printf("Task1 create failed\r\n");
    }

    stTask.pfnTaskEntry = (TSK_ENTRY_FUNC)TaskSampleEntry2;
    stTask.uwStackSize = 0x1000;
    stTask.pcName = "TaskSampleEntry2";
    stTask.usTaskPrio = 7; /* Os task priority is 7 */
    uwRet = LOS_TaskCreate(&taskID2, &stTask);
    if (uwRet != LOS_OK) {
        printf("Task2 create failed\r\n");
    }
	
}


#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
